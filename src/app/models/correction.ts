export class Correction {
    public id: number;

    constructor(
        public project: string,
        public date: Date,
        public value: number
    ) {}
}
