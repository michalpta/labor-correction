import { Component, OnInit, Input } from '@angular/core';
import { CorrectionService } from '../../correction.service';
import { Correction } from '../../models/correction';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-correction',
  templateUrl: './correction.component.html',
  styleUrls: ['./correction.component.css']
})
export class CorrectionComponent implements OnInit {

  @Input() project: string;
  @Input() date: Date;

  correction: Correction;

  constructor(private correctionService: CorrectionService) { }

  ngOnInit() {
    this.correction = this.correctionService
      .getCorrection(this.project, this.date);
  }

  updateCorrection(event: any) {
    this.correctionService.updateCorrection(this.correction);
  }

}
