import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CorrectionComponent } from './components/correction/correction.component';
import { CorrectionService } from './correction.service';
import { CorrectionFormComponent } from './components/correction-form/correction-form.component';


@NgModule({
  declarations: [
    AppComponent,
    CorrectionComponent,
    CorrectionFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [CorrectionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
